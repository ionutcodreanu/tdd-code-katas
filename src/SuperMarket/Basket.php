<?php

namespace TddKata\SuperMarket;

class Basket
{
    /** @var Product[] */
    private $products = [];
    private $productQuantity = [];

    public function getProducts()
    {
        return $this->products;
    }

    public function addProduct(Product $product, float $quantity): void
    {
        $productKey = $product->getName();
        $this->products[$productKey] = $product;
        if (false === array_key_exists($productKey, $this->productQuantity)) {
            $this->productQuantity[$productKey] = $quantity;
        } else {
            $this->productQuantity[$productKey] += $quantity;
        }
    }

    public function getCost(): float
    {
        $cost = 0;
        foreach ($this->products as $product) {
            $cost += $this->getProductQuantity($product->getName()) * $product->getCost();
        }
        return $cost;
    }

    public function getProductQuantity($product): float
    {
        if (false === array_key_exists($product, $this->productQuantity)) {
            return 0;
        }
        return $this->productQuantity[$product];
    }
}
