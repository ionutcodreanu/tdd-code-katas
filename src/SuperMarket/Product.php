<?php

namespace TddKata\SuperMarket;

class Product
{
    /** @var  string */
    private $name;
    /** @var  float */
    private $price;
    /** @var  float */
    private $discount = 0;

    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param float $price
     * @throws InvalidPrice
     */
    public function setPrice(float $price)
    {
        if ($price <= 0) {
            throw new InvalidPrice("Price should be greater or equals to 0");
        }
        $this->price = $price;
    }

    /**
     * @param float $discount
     * @throws InvalidDiscount
     */
    public function setDiscount(float $discount)
    {
        if ($discount < 0 || $discount > 1) {
            throw new InvalidDiscount("Discount should be between 0 and 1");
        }
        $this->discount = $discount;
    }

    /**
     * @return float
     */
    public function getCost(): float
    {
        return (1 - $this->discount) * $this->price;
    }
}
