<?php

namespace TddKata\SuperMarket;

class Shopper
{
    /** @var Basket */
    private $basket;

    public function __construct()
    {
        $this->basket = new Basket();
    }

    public function getBasket(): Basket
    {
        return $this->basket;
    }

    public function addProduct(Product $product, float $quantity = 1)
    {
        $this->basket->addProduct($product, $quantity);
    }
}
