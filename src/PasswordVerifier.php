<?php

namespace TddKata;

class PasswordVerifier
{
    private $errors = [];

    public function verify($password): bool
    {
        if (is_null($password)) {
            $this->addError('nullPassword', 'Password can not be a null value');
        }
        if (strlen($password) <= 8) {
            $this->addError('passwordLength', 'Password should be larger than 8 characters');
        }

        $hasNumber = $this->hasUpperCase($password);
        if (false === $hasNumber) {
            $this->addError(
                'noUpperCaseLetter',
                'The password should contain at least one upper case letter'
            );
        }

        $hasNumber = $this->hasNumber($password);
        if (false === $hasNumber) {
            $this->addError(
                'noNumber',
                'The password should contain at least one number'
            );
        }

        $hasLowerCase = $this->hasLowerCase($password);
        if (false === $hasLowerCase) {
            $this->addError(
                'noLowerCaseLetter',
                'The password should contain at least one lower case letter'
            );

            return false;
        }

        if (count($this->getErrors()) > 2) {
            return false;
        }

        return true;
    }

    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * @param $errorMessage
     * @param $errorCode
     */
    private function addError($errorCode, $errorMessage): void
    {
        $this->errors[$errorCode] = $errorMessage;
    }

    /**
     * @param $password
     * @return bool
     */
    private function hasUpperCase($password): bool
    {
        $regularExpressionUpperCase = '/[A-Z]+/';
        $hasUpperCase = preg_match($regularExpressionUpperCase, $password);
        return (bool)$hasUpperCase;
    }

    /**
     * @param $password
     * @return bool
     */
    private function hasLowerCase($password): bool
    {
        $regularExpressionLowerCase = '/[a-z]+/';
        $hasUpperCase = preg_match($regularExpressionLowerCase, $password);
        return (bool)$hasUpperCase;
    }

    /**
     * @param $password
     * @return bool
     */
    private function hasNumber($password): bool
    {
        $regularExpressionNumber = '/[0-9]+/';
        $hasNumber = preg_match($regularExpressionNumber, $password);
        return (bool)$hasNumber;
    }
}
