<?php

namespace TddKata;

class StringSum
{
    public function add(string $string)
    {
        if ($string === "") {
            return 0;
        }
        $operators = $this->getOperators($string);
        $this->checkNegatives($operators);

        $operatorsLessThanLimit = $this->filterOperatorsGreatherThanLimit($operators);
        $sum = $this->sum($operatorsLessThanLimit);

        return $sum;
    }

    private function computeDelimiter($string)
    {
        $pattern = '/\/\/(\[.*\])\n{1}(.*)/';
        $matched = preg_match($pattern, $string, $matches);
        if ($matched !== false && $matched >= 1) {
            $patternDelimiters = '/(?:\[(.*)\])/';
            $operators = $matches[2];

            preg_match($patternDelimiters, $matches[1], $matches);
            $delimiters = $matches[1];
            return [explode('][', $delimiters), $operators];
        }

        $pattern = '/\/\/(.*)\n{1}(.*)/';
        $matched = preg_match($pattern, $string, $matches);
        if ($matched === 0 || $matched === false) {
            return [',', $string];
        }

        return [$matches[1], $matches[2]];
    }

    /**
     * @param $operators
     */
    public function checkNegatives($operators): void
    {
        $negatives = array_filter($operators, function ($operator) {
            if ((int)$operator < 0) {
                return true;
            }
            return false;
        });

        if (count($negatives)) {
            $negativesString = implode(",", $negatives);
            throw new \InvalidArgumentException("Negative numbers [$negativesString] are not allowed");
        }
    }

    /**
     * @param string $string
     * @return array
     */
    public function getOperators(string $string): array
    {
        list($delimiter, $sumString) = $this->computeDelimiter($string);
        $replaceOtherDelimiters = ["\n"];
        if (count($delimiter) > 1) {
            $finalDelimiter = array_shift($delimiter);
            $replaceOtherDelimiters = array_merge($replaceOtherDelimiters, $delimiter);
        } else {
            $finalDelimiter = is_array($delimiter) ? $delimiter[0] : $delimiter;
        }
        $stringReplaced = str_replace($replaceOtherDelimiters, $finalDelimiter, $sumString);
        $operators = explode($finalDelimiter, $stringReplaced);
        return $operators;
    }

    /**
     * @param $operators
     * @return int
     */
    public function sum($operators): int
    {
        $sum = 0;
        foreach ($operators as $operator) {
            $sum += (int)$operator;
        }
        return $sum;
    }

    /**
     * @param $operators
     * @return array
     */
    public function filterOperatorsGreatherThanLimit($operators): array
    {
        $operatorsLessThanLimit = array_filter($operators, function ($operator) {
            if ($operator > 1000) {
                return false;
            }
            return true;
        });
        return $operatorsLessThanLimit;
    }
}
