<?php

namespace StringCalculator\Tests\Unit;

use PHPUnit\Framework\TestCase;
use TddKata\PasswordVerifier;

class PasswordVerifierTest extends TestCase
{
    /**
     * @var PasswordVerifier
     */
    private $passwordVerifier;

    protected function setUp()
    {
        $this->passwordVerifier = new PasswordVerifier();
    }


    /**
     * given a null password then will return an error message
     */
    public function testGivenANullPasswordThenWillReturnAnErrorMessage()
    {
        list($valid, $errors) = $this->verifyPassword(null);
        static::assertFalse($valid);
        static::assertArrayHasKey('nullPassword', $errors);
        static::assertEquals('Password can not be a null value', $errors['nullPassword']);
    }

    /**
     * given a not empty string with length of eight characters will return an password length error message
     */
    public function testGivenANotEmptyStringWithLengthOfEightCharactersWillReturnAnPasswordLengthErrorMessage()
    {
        list($valid, $errors) = $this->verifyPassword("-------");
        static::assertFalse($valid);
        static::assertArrayHasKey('passwordLength', $errors);
        static::assertEquals('Password should be larger than 8 characters', $errors['passwordLength']);
    }

    /**
     * given a password with no upper case letter then a no upper case error will be returned
     */
    public function testGivenAPasswordWithNoUpperCaseLetterThenANoUpperCaseErrorWillBeReturned()
    {
        list($valid, $errors) = $this->verifyPassword("----");
        static::assertFalse($valid);
        static::assertArrayHasKey('noUpperCaseLetter', $errors);
        $expectedMessage = 'The password should contain at least one upper case letter';
        static::assertEquals($expectedMessage, $errors['noUpperCaseLetter']);
    }

    /**
     * given a password with no lower case letter then a no lower case error will be returned
     */
    public function testGivenAPasswordWithNoLowerCaseLetterThenANoLowerCaseErrorWillBeReturned()
    {
        list($valid, $errors) = $this->verifyPassword("----");
        static::assertFalse($valid);
        static::assertArrayHasKey('noLowerCaseLetter', $errors);
        $expectedMessage = 'The password should contain at least one lower case letter';
        static::assertEquals($expectedMessage, $errors['noLowerCaseLetter']);
    }

    /**
     * given a password with no numeric letter then a no numeric letter error will be returned
     */
    public function testGivenAPasswordWithNoNumericLetterThenANoNumericLetterErrorWillBeReturned()
    {
        list($valid, $errors) = $this->verifyPassword("--------");
        static::assertFalse($valid);
        static::assertArrayHasKey('noNumber', $errors);
        $expectedMessage = 'The password should contain at least one number';
        static::assertEquals($expectedMessage, $errors['noNumber']);
    }


    /**
     * given a password with length of nine characters then the password will be valid
     */
    public function testGivenAPasswordWithLengthOfNineCharactersThenThePasswordWillBeValid()
    {
        list($valid, $errors) = $this->verifyPassword("12345678Aa");
        static::assertTrue($valid);
        static::assertCount(0, $errors);
    }

    /**
     * given a password with three valid rules then the password will be valid
     */
    public function testGivenAPasswordWithThreeValidRulesThenThePasswordWillBeValid()
    {
        list($valid, $errors) = $this->verifyPassword("aB3");
        static::assertTrue($valid);
    }


    /**
     * given a password with two valid rules then the password will not be validated
     */
    public function testGivenAPasswordWithTwoValidRulesThenThePasswordWillNotBeValidated()
    {
        list($valid, $errors) = $this->verifyPassword("a");
        static::assertFalse($valid);
    }

    /**
     * given a password with four valid rules and no lower case then the password will be invalid
     */
    public function testGivenAPasswordWithFourValidRulesAndNoLowerCaseThenThePasswordWillBeInvalid()
    {
        list($valid, $errors) = $this->verifyPassword("12345678A");
        static::assertFalse($valid);
        static::assertArrayHasKey('noLowerCaseLetter', $errors);
        $expectedMessage = 'The password should contain at least one lower case letter';
        static::assertEquals($expectedMessage, $errors['noLowerCaseLetter']);
    }




    /**
     * @param $password
     * @return array
     */
    private function verifyPassword($password): array
    {
        $valid = $this->passwordVerifier->verify($password);
        $errors = $this->passwordVerifier->getErrors();
        return array($valid, $errors);
    }
}
