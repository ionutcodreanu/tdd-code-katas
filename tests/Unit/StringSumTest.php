<?php

namespace TddKata\Tests\Unit;

use TddKata\StringSum;
use PHPUnit\Framework\TestCase;

class StringSumTest extends TestCase
{
    /**
     * @var StringSum
     */
    private $calculator;

    /**
     * @param $string
     * @return int
     */
    public function sum($string): int
    {
        return $this->calculator->add($string);
    }

    protected function setUp()
    {
        $this->calculator = new StringSum();
    }

    /**
     * given empty string return zero
     */
    public function testGivenEmptyStringReturnZero()
    {
        $string = "";
        static::assertEquals(0, $this->sum($string));
    }

    /**
     * given a string with value one value return one
     */
    public function testGivenAStringWithValueOneValueReturnOne()
    {
        $string = "1";
        static::assertEquals(1, $this->sum($string));
    }

    /**
     * given a string with value two return two
     */
    public function testGivenAStringWithValueTwoReturnTwo()
    {
        $string = "2";
        static::assertEquals(2, $this->sum($string));
    }

    /**
     * given a string with value one and value two separated by spaces return three
     */
    public function testGivenAStringWithValueOneAndValueTwoSeparatedBySpacesReturnThree()
    {
        $string = "1,2";
        static::assertEquals(3, $this->sum($string));
    }

    /**
     * given a string with three values return their sum
     */
    public function testGivenAStringWithThreeValuesReturnTheirSum()
    {
        $string = "1,2,3";
        static::assertEquals(6, $this->sum($string));
    }

    /**
     * given a string with five numbers return their sum
     */
    public function testGivenAStringWithFiveNumbersReturnTheirSum()
    {
        $string = "1,2,3,4,5";
        static::assertEquals(15, $this->sum($string));
    }

    /**
     * given a string with three numbers separted by new line return their sum
     */
    public function testGivenAStringWithThreeNumbersSepartedByNewLineReturnTheirSum()
    {
        $string = "1\n2,3";
        static::assertEquals(6, $this->sum($string));
    }

    /**
     * given a string with a specific delimiter return their sum
     */
    public function testGivenAStringWithASpecificDelimiterReturnTheirSum()
    {
        $string = "//;\n1;2;3";
        static::assertEquals(6, $this->sum($string));
    }

    /**
     * given a string with negative operators return an exception with numbers in the message
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage Negative numbers [-1,-2] are not allowed
     */
    public function testGivenAStringWithNegativeOperatorsReturnAnExceptionWithNumbersInTheMessage()
    {
        $string = "1,2,3,-1,4,5,-2,6";
        $this->sum($string);
    }

    /**
     * given a string with two numbers bigger than 1000 will return a sum that ignore them
     */
    public function testGivenAStringWithTwoNumbersBiggerThan1000WillReturnASumThatIgnoreThem()
    {
        $string = "1,1001,1000,2,10002,3,5222";
        static::assertEquals(1006, $this->sum($string));
    }

    /**
     * given a string with delimiter of length three will return their sum
     */
    public function testGivenAStringWithDelimiterOfLengthThreeWillReturnTheirSum()
    {
        $string = "//[***]\n1***2***5";
        static::assertEquals(8, $this->sum($string));
    }

    /**
     * given a string with two delimiters will return the sum
     */
    public function testGivenAStringWithTwoDelimitersWillReturnTheSum()
    {
        $string = "//[*][%]\n4*5*6%10%20*25";
        static::assertEquals(70, $this->sum($string));
    }

    /**
     * given a string with two delimiters of length 3 will return their sum
     */
    public function testGivenAStringWithTwoDelimitersOfLength3WillReturnTheirSum()
    {
        $string = "//[***][%%%]\n4***5***6%%%40%%%20***25";
        static::assertEquals(100, $this->sum($string));
    }
}
