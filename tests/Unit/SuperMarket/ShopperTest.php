<?php

namespace TddKata\Tests\Unit\SuperMarket;

use PHPUnit\Framework\TestCase;
use TddKata\SuperMarket\Basket;
use TddKata\SuperMarket\Product;
use TddKata\SuperMarket\Shopper;

class ShopperTest extends TestCase
{
    /**
     * @var Shopper
     */
    private $shopper;

    protected function setUp()
    {
        $this->shopper = new Shopper();
    }

    /**
     * given i am shopper i can get a basket
     */
    public function testGivenIAmShopperICanGetABasket()
    {
        static::assertInstanceOf(Basket::class, $this->shopper->getBasket());
    }

    /**
     * given i am a shopper and i add two products then the basket should contain the products
     */
    public function testGivenIAmAShopperAndIAddTwoProductsThenTheBasketShouldContainTheProducts()
    {
        $firstProductName = "product 1";
        $secondProductName = "product 2";

        $firstProduct = new Product($firstProductName);
        $firstProduct->setPrice(1);

        $secondProduct = new Product($secondProductName);
        $secondProduct->setPrice(1);


        $this->shopper->addProduct($firstProduct);
        $this->shopper->addProduct($secondProduct);

        $basket = $this->shopper->getBasket();
        static::assertCount(2, $basket->getProducts());
    }

    /**
     * given i am a shopper and i add one product with price of 10 dollars then the basket should cost 10 dollars
     */
    public function testGivenIAmAShopperAndIAddOneProductWithPriceOf10DollarsThenTheBasketShouldCost10Dollars()
    {
        $product = new Product("product 3");
        $product->setPrice(10);

        $this->shopper->addProduct($product, 1);
        $basket = $this->shopper->getBasket();
        static::assertCount(1, $basket->getProducts());
        static::assertEquals(10, $basket->getCost());
    }

    /**
     * given i am a shopper and i add two products with price of 10 dollars each then the basket should cost 20 dollars
     */
    public function testGivenIAmAShopperAndIAddTwoProductsWithPriceOf10DollarsEachThenTheBasketShouldCost20Dollars()
    {
        $firstProductName = "product 4";
        $secondProductName = "product 5";

        $firstProduct = new Product($firstProductName);
        $firstProduct->setPrice(10);

        $secondProduct = new Product($secondProductName);
        $secondProduct->setPrice(10);

        $this->shopper->addProduct($firstProduct, 1);
        $this->shopper->addProduct($secondProduct, 1);
        $basket = $this->shopper->getBasket();
        static::assertCount(2, $basket->getProducts());
        static::assertEquals(20, $basket->getCost());
        static::assertEquals(1, $basket->getProductQuantity($firstProductName));
        static::assertEquals(1, $basket->getProductQuantity($secondProductName));
    }

    /**
     * given i am a shopper and i add the product twice then the basket should contain first product with quantity 2
     */
    public function testGivenIAmAShopperAndIAddTheProductTwiceThenTheBasketShouldContainFirstProductWithQuantity2()
    {
        $productName = "product 6";
        $product = new Product($productName);
        $product->setPrice(10);


        $this->shopper->addProduct($product);
        $this->shopper->addProduct($product);
        $basket = $this->shopper->getBasket();
        static::assertEquals(2, $basket->getProductQuantity($productName));
    }
}
