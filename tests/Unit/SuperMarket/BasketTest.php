<?php

namespace TddKata\Tests\Unit\SuperMarket;

use TddKata\SuperMarket\Basket;
use PHPUnit\Framework\TestCase;
use TddKata\SuperMarket\Product;

class BasketTest extends TestCase
{
    /** @var  Basket */
    private $basket;

    protected function setUp()
    {
        $this->basket = new Basket();
    }

    /**
     * given i have a basket with no products then checking quantity for a non existing product will return 0
     */
    public function testGivenIHaveABasketWithNoProductsThenCheckingQuantityForANonExistingProductWillReturn0()
    {
        static::assertEquals(0, $this->basket->getProductQuantity("no basket product"));
    }

    /**
     * given i add a product with quantity three and unit price 100 then the basket should cost 300
     */
    public function testGivenIAddAProductWithQuantityThreeAndUnitPrice100ThenTheBasketShouldCost300()
    {
        $product = new Product("product 1");
        $product->setPrice(100);

        $this->basket->addProduct($product, 3);
        static::assertEquals(300, $this->basket->getCost());
    }


    /**
     * given i add to basket a product with a discount promotion of twenty percent then the promotion should be deduct from basket cost
     */
    public function testGivenIAddToBasketAProductWithADiscountPromotionOfTwentyPercentThenThePromotionShouldBeDeductFromBasketCost(
    )
    {
        $product = new Product("product 1");
        $product->setPrice(100);
        $product->setDiscount(0.20);

        $this->basket->addProduct($product, 1);
        static::assertEquals(80, $this->basket->getCost());
    }

    /**
     * given i add a product with full discount then the basket cost should be 0
     */
    public function testGivenIAddAProductWithFullDiscountThenTheBasketCostShouldBe0()
    {
        $product = new Product("product 1");
        $product->setPrice(100);
        $product->setDiscount(1);

        $this->basket->addProduct($product, 1);
        static::assertEquals(0, $this->basket->getCost());
    }
}
