<?php

namespace TddKata\Tests\Unit\SuperMarket;

use PHPUnit\Framework\TestCase;
use TddKata\SuperMarket\InvalidDiscount;
use TddKata\SuperMarket\InvalidPrice;
use TddKata\SuperMarket\Product;

class ProductTest extends TestCase
{
    /** @var  Product */
    private $product;

    protected function setUp()
    {
        $this->product = new Product("unit test");
    }


    /**
     * set a negative price will throw an invalid price exception
     */
    public function testSetANegativePriceWillThrowAnInvalidPriceException()
    {
        static::expectException(InvalidPrice::class);
        static::expectExceptionMessage("Price should be greater or equals to 0");

        $this->product->setPrice(-1);
    }

    /**
     * set a 0 price will throw an invalid price exception
     */
    public function testSetA0PriceWillThrowAnInvalidPriceException()
    {
        static::expectException(InvalidPrice::class);
        static::expectExceptionMessage("Price should be greater or equals to 0");

        $this->product->setPrice(0);
    }


    /**
     * given a discount less than 0 will throw an invalid discount exception
     */
    public function testGivenADiscountLessThan0WillThrowAnInvalidDiscountException()
    {
        static::expectException(InvalidDiscount::class);
        static::expectExceptionMessage("Discount should be between 0 and 1");

        $this->product->setDiscount(-0.5);
    }

    /**
     * given a discount of 0 then the product cost should be equals with the product price
     */
    public function testGivenADiscountOf0ThenTheProductCostShouldBeEqualsWithTheProductPrice()
    {
        $this->product->setPrice(100);
        $this->product->setDiscount(0);
        static::assertEquals(100, $this->product->getCost());
    }
}
