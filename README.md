## TDD Kata 1 String Calculator

[Source](http://osherove.com/tdd-kata-1/)

The following is a TDD Kata- an exercise in coding, refactoring and test-first, that you should apply daily for at least 15 minutes (I do 30).3

### Before you start:

* Try not to read ahead1
* Do one task at a time. The trick is to learn to work incrementally.
* Make sure you only test for correct inputs. there is no need to test for invalid inputs for this kata

### String Calculator

1. Create a simple String calculator with a method int Add(string numbers)
1.1 The method can take 0, 1 or 2 numbers, and will return their sum (for an empty string it will return 0) for example “” or “1” or “1,2”
1.2 Start with the simplest test case of an empty string and move to 1 and two numbers
1.3 Remember to solve things as simply as possible so that you force yourself to write tests you did not think about
1.4 Remember to refactor after each passing test
2. Allow the Add method to handle an unknown amount of numbers
3. Allow the Add method to handle new lines between numbers (instead of commas).
3.1 the following input is ok:  “1\n2,3”  (will equal 6)
3.2 the following input is NOT ok:  “1,\n” (not need to prove it - just clarifying)
4. Support different delimiters
4.1 to change a delimiter, the beginning of the string will contain a separate line that looks like this:   “/1/[delimiter]\n[numbers…]” for example “//;\n1;2” should return three where the default delimiter is ‘;’ .
4.2 the first line is optional. all existing scenarios should still be supported
5. Calling Add with a negative number will throw an exception “negatives not allowed” - and the negative that was passed.if there are multiple negatives, show all of them in the exception message
6. Numbers bigger than 1000 should be ignored, so adding 2 + 1001  = 2
7. Delimiters can be of any length with the following format:  “//[delimiter]\n” for example: “//[***]\n1***2***3” should return 6
8. Allow multiple delimiters like this:  “//[delim1][delim2]\n” for example “//[*][%]\n1*2%3” should return 6.
9. make sure you can also handle multiple delimiters with length longer than one char

## Password verifier

[Source](http://osherove.com/tdd-kata-3-refactoring/)

Create a Password verifications class called “PasswordVerifier”.
1. Add the following verifications to a master function called “Verify()”
1.1 password should be larger than 8 chars
1.2 password should not be null1
1.3 password should have one uppercase letter at least
1.4 password should have one lowercase letter at least
1.5 password should have one number at least
2. Each one of these should throw an exception with a different message of your choosing
3. Add feature: Password is OK if at least three of the previous conditions is true
4. Add feature: password is never OK if item 1.4 is not true.
5. Assume Each verification takes 1 second to complete. How would you solve  items 3 and 4  so tests can run faster?


## Kata01: Supermarket Pricing

[Source](http://codekata.com/kata/kata01-supermarket-pricing/)